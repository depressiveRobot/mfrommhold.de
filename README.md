# 🤖 mfrommhold.de

This is the source code of my personal website hosted at https://mfrommhold.de.

## Thanks

- based on [John Doe](https://github.com/cadars/john-doe) website theme

## License

© Marvin Frommhold
